package kyyro.hci.password;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import android.content.Context;
import android.gesture.Gesture;
import android.gesture.GestureStore;
import android.util.Log;
import android.widget.Toast;

/*
 * container class for settings
 */
public final class MySettings {
	public String username;
	public String password;
	private Gesture gesture;	// private to make easy to check if changed
	private float pathThresh;
	private float endThresh;
	public boolean background;
	public String backgroundPath;
	public float precision;
	
	boolean valid;
	//boolean gestureChanged;	// track when gesture is changed
	
	static final int ERR = -2;
	static final int OKAY = 0;
	static final float MIN_THRESH = 40.0f;
	static final float MAX_THRESH = 75.0f;
	static final float PATH_THRESH_SCALE = 1.5f;
	
	private static final String TAG = "MySettings";
	private static final boolean DEBUG = false;
	
	/*
	 * Creates null settings
	 */
	public MySettings() {
		reset();
		gesture = null;
	}
	
	public Gesture getGesture() {
		return gesture;
	}
	
	public void setGesture(Gesture g) {
		valid = (g != null) && (g.getLength() > 0);
		gesture = g;
	}
	
	public float getPathThresh() {
		return pathThresh;
	}
	
	public float getEndThresh() {
		return endThresh;
	}
	
	public float getPrecision() {
		return precision;
	}
	
	public void setPathThresh(float val) {
		pathThresh = val;
	}
	
	public void setEndThresh(float val) {
		endThresh = val;
	}
	
	public void setPrecision(float val) {
		if(val <= 1 && val >= 0)
			precision = val;
		
		pathThresh = precision*(MAX_THRESH - MIN_THRESH)+MIN_THRESH;
		endThresh = pathThresh*PATH_THRESH_SCALE;
	}
	
	
	public boolean isValid(){
		return (gesture != null) && (gesture.getLength() > 0);
	}
	
	/*
	 * Write settings to internal storage.
	 * Returns in in range [-2,0] indicating
	 * how if the save suceeded (0), one file
	 * failed to write(-1), or both (-2).
	 */
	public int save(Context context) {
		int result = 0;
		
		// write string values
		try {
			// for encryption
			FileOutputStream fout = 
				context.openFileOutput("settings", Context.MODE_PRIVATE); 
			PrintWriter out = new PrintWriter(new FileWriter(fout.getFD()));
			
			if(DEBUG)
				Toast.makeText(context, context.getFilesDir().toString(), 
					Toast.LENGTH_SHORT).show();
			
			// write "encrypted" values to file
			out.println(username);
			out.println(password);
			out.println(Float.toString(precision));
			out.println(Boolean.toString(background));
			out.println(backgroundPath);
			out.close();
			
		} catch (IOException ioex) {
			Log.e(TAG, "Failed to open file to save settings", ioex);
			result--;
		}
			
		// write gesture
		if(gesture == null) {
			context.deleteFile("gesture");
		} else {
		
			try {
				GestureStore store = new GestureStore();
				store.addGesture(PasswordManagerActivity.GESTURE, gesture);
				store.save(context.openFileOutput("gesture", 
					Context.MODE_PRIVATE));
				
			} catch (IOException e) {
				Log.e(TAG, "Gesture save failed", e);
				result--;
			}
		}
		
		return result;
	}
	
	/*
	 * Read settings from internal storage.
	 * returns an int in range [-5,0] indicating
	 * the number of unset settings, besides background
	 * and scaling.
	 */
	public int load(Context context) {
		
		int result = -2;
		boolean settingFileExists = false;
		boolean gestureFileExists = false;
		String[] files = context.fileList();
		
		// check which files exists
		for( int i = 0; i < files.length; i++){
			if(files[i].equalsIgnoreCase("settings"))
				settingFileExists = true;
			else if(files[i].equalsIgnoreCase("gesture"))
				gestureFileExists = true;
		}
		
		// read string-valued settings if file exists
		if(settingFileExists){
			if(DEBUG)
				Toast.makeText(context, "settings found", 0).show();
			loadSettings(context);
			result++;
		} else {
			reset();
		}
		
		if(gestureFileExists) {
			if(DEBUG)
				Toast.makeText(context, "gesture found", 0).show();
			loadGesture(context);
			result++;
			valid = true; // if gesture is set, it's usable
		} else {
			gesture = null;
			valid = false;
		}
		
		
		return result;
	}
	
	/*
	 * Load just the settings
	 */
	public void loadSettings(Context context) {
		
		try {
			// for decryption
			FileInputStream fin = context.openFileInput("settings");
			BufferedReader in = new BufferedReader(new FileReader(fin.getFD()));			
			reset();	// reset settings
			
			// read and store
			username = in.readLine();
			password = in.readLine();
			setPrecision(Float.parseFloat(in.readLine()));
			background = Boolean.parseBoolean(in.readLine());
			backgroundPath = in.readLine();
			
			in.close();
			
		} catch(FileNotFoundException fnfex) {
			Log.e(TAG, "Settings file not found.", fnfex);
			
		} catch (IOException ioex) {
			Log.e(TAG, "IO failed on settings file", ioex);
		}
	}
	
	/*
	 * Load just the gesture
	 */
	public void loadGesture(Context context) {
		
		try {
			GestureStore store = new GestureStore();
			store.load(context.openFileInput("gesture"), true);
			gesture = store.getGestures("gesture").get(0);
		} catch (IOException e) {
			Log.e(TAG, "Failed to read gesture", e);
		}
	}
	
	/*
	 * count number of settings that have been set
	 */
	private int count() {
		int result = 0;
		
		if(username != null)
			result++;
		if(password != null)
			result++;
		if(gesture != null)
			result++;
		if(backgroundPath != null)
			result++;
		/*if(pathThresh >= MIN_THRESH && pathThresh <= MAX_THRESH)
			result++;
		if(pathThresh >= MIN_THRESH && pathThresh <= MAX_THRESH)
			result++;*/
				
		return result;
	}
	
	public void reset() {
		username = null;
		password = null;
		background = false;
		backgroundPath = null;
		setPrecision(0.5f);
		
		valid = false;
	}
}


