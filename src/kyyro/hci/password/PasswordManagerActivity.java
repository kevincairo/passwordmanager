/* Kevin Kyyro
 * HCI Project #3 - Gestural Password Manager Mock-up for Android
 * PasswordManagerActivity.java
 * 
 * Description: This is the tentative "Start" of the app. This is
 * the gesture input page. Press options key to configure. 
 * -Should not allow input if not configured, should give message 
 * -Should at least give a brief indication of success or failure 
 * --------------------------------------------------------------
 * -parameters: thresh 35, end thresh 50
 * 		-note: path should be tighter, end points can be a looser
 * 
 * TO-DO:
 * -complete onStart()
 * 		-load saved settings if there are any
 * -flesh out feedback
 * 		-incomplete configuration
 * 		-gesture match/mismatch
 * 
 * -report (2 pages, single spaced, 30 points):
 *  	*Task Analysis (5 points)
 * 		*User Level (Novice/Intermediate/Expert) Analysis (5 points)
 * 		*Applicability of 8 golden rules (15 points)
 * 		*Interface style choice (5 points)
 * 
 * WISHLIST:
 * -first time tutorial
 * -add in app browser so that users can actually use gestures to
 *  sign into UF wifi
 * 		-requires adding some reasonable amount of encryption to the 
 *  	stored passwords...
 * 
 */

package kyyro.hci.password;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.Math;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.gesture.Gesture;
import android.gesture.GestureOverlayView;
import android.gesture.GestureOverlayView.OnGestureListener;
import android.gesture.GestureStroke;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class PasswordManagerActivity extends Activity 
	implements OnGestureListener, OnClickListener {
    
	public static final String TAG = "PasswordManagerActivity";
	
	// request code and extra names for configuration activity
	public static final int CONFIG = 42;
	public static final int SET_GESTURE = 43;
	public static final int SET_BACKGROUND = 44;
	public static final String USERNAME = "username";
	public static final String PASSWORD = "password";
	public static final String GESTURE = "gesture";
	public static final String PATH_THRESH = "path threshold";
	public static final String END_THRESH = "end threshold";
	public static final String BACKGROUND = "background uri";
	public static final String SCALING = "background scaling";
	
	// Settings
	private MySettings mSettings;
	private boolean DEBUG = false;
	private boolean CLEAN = false;
	private boolean DB_LOGIN = false;
	
	// metrics
	public static final String TRIES = "Tries";
	public static final String TIME_DATE = "TimeDate";
	public static final String GESTURES_COUNT = "GestureCount";
	public static final String OLD_GESTURE_LENGTH = "OldGestureLength";
	public static final String PRECISIONS = "Precisions";
	private static final String FIRST_LOAD = "FirstLoad";
	private int tries = 0;
	private int gestureCount = 0;
	private ArrayList<Float> precisions;
	
	private int oldTries = 0;
	private int oldGestureCount = 0;
	private float oldGestureLength = 0.0f;
	private float oldPrecision = 0.0f;
	private boolean firstLoad = false;
	private boolean unconfigured = false;
	
	// UI elements
	GestureOverlayView gestureOverlay;
	private Button enterButton;
	private Button clearButton;
	private boolean startUp = false;
	private String bg;
	private Bitmap bitmap;
	private BitmapDrawable bd;
	
	// INCOMPLETE -- handle background images, load settings
	/*
	 * OnCreate is responsible for setting up UI and bindings
	 * 
	 */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.input);
        
        // set up data
        mSettings = new MySettings();
        startUp = true;
        bg = "";
        bitmap = null;
        
        // set up UI
        gestureOverlay = 
        	(GestureOverlayView)findViewById(R.id.input_gesture);
        gestureOverlay.addOnGestureListener(this);
        gestureOverlay.setFadeOffset(10000);
        gestureOverlay.setGestureStrokeLengthThreshold(0);
        gestureOverlay.setGestureStrokeAngleThreshold(0);
        gestureOverlay.setGestureStrokeSquarenessTreshold(0);
        gestureOverlay.setGestureStrokeType(1);
        
        enterButton = (Button) findViewById(R.id.input_enter_button);
        enterButton.setOnClickListener(this);
        enterButton.setClickable(false);
        enterButton.setTextColor(R.color.unclickable);
        clearButton = (Button) findViewById(R.id.input_clear_button);
        clearButton.setOnClickListener(this);
        
        if(fileExists("reset") || 
        		(!fileExists("settings") && !fileExists("gesture")))
        	firstLoad = true;
        
        precisions = new ArrayList<Float>();
        
        // delete reset file
    	deleteFile("reset");
    }
    
    @Override
    protected void onStart() {
    	super.onStart();
    	
    	// load settings
    	int loadResult = mSettings.load(this);
    	
    	if(DEBUG) Toast.makeText(this, "Load result = " + loadResult, 0).show();
    	
    	// show alertDialog if opening app and gesture not configured
    	if(mSettings.valid == false && startUp) {
    		if(DEBUG) Toast.makeText(this, "no settings loaded", 0).show();
    		
    		AlertDialog configAlert = new AlertDialog.Builder(this).create();
			configAlert.setTitle("Welcome");
			configAlert.setMessage("Would you like to configure your gesture to get started?");
			configAlert.setButton("Okay", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int which) {
					if(which == DialogInterface.BUTTON1)
						openConfiguration();
				}
			});
			
			configAlert.setButton2("No thanks", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {}
			});
			
			configAlert.show();
        }        
    	
    	// display background if set
    	if(mSettings.background) {
    		if(DEBUG) Toast.makeText(this, "Background path set", 0).show();
    		
    		// load bitmap
    		bg = mSettings.backgroundPath;
	    	bitmap = BitmapFactory.decodeFile(bg);
	    	bd = new BitmapDrawable(getResources(), bitmap);
    		gestureOverlay.setBackgroundDrawable(bd);
    		
    	} else {
    		// set default background
    		gestureOverlay.setBackgroundResource(R.drawable.input_gradient);
    	}
    }
    
    @Override
    protected void onDestroy() {
    	super.onDestroy();
    	
    	// if clean, remove configuration when exitting app
    	if(CLEAN){
        	this.deleteFile("settings");
        	this.deleteFile("gesture");
        }
    }
    
    @Override
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        // delete background resource to free memory
        if(bitmap != null) {
        	bitmap.recycle();
        	bd = null;
        }
        
        oldTries = tries;
        oldPrecision = mSettings.precision;
        oldGestureCount = gestureCount;
        
        if(mSettings.getGesture() != null)
        	oldGestureLength = mSettings.getGesture().getLength();
        else
        	oldGestureLength = 0;                
    }
    
    protected void onResume() {
    	super.onResume();
    	
    	// if not after initial load, count relevant settings changes
        if(!firstLoad && !startUp) {
        	if(oldPrecision != mSettings.precision)
        		precisions.add(oldPrecision);
        	
        	if(mSettings.getGesture() != null)
        		if(mSettings.getGesture().getLength() != oldGestureLength)
        			gestureCount++;
        }
        
        // this is called on start up, want firstLoad to become false after
        // returning to this activity from configuration
        if(startUp == false)
        	firstLoad = false;
        	
    	// any time after this has executed, no longer on start up
    	startUp = false;
    }
    
    /* 
     * Compare inputGesture to saved gesture, return true if matches
     */
    boolean compareGestures(Gesture inputGesture) {
    	Gesture savedGesture = mSettings.getGesture();
    	
    	// compare both gestures against the other because the input
    	// gesture could "match" the saved gesture if the saved gesture's
    	// starting and ending points are close together, for example
    	return compareGestures(inputGesture, savedGesture) && 
    			compareGestures(savedGesture, inputGesture);
    }
    
	/* 
	 * Compare testGesture against againstGesture 
	 */
	boolean compareGestures(Gesture testGesture, Gesture againstGesture) {
		if(mSettings.getGesture() == null || testGesture == null)
			return false;
		
		Iterator<GestureStroke> inStroke = testGesture.getStrokes().iterator();
		Iterator<GestureStroke> sStroke = againstGesture.getStrokes().iterator();
		
		// for each stroke of both gestures
		while(inStroke.hasNext() && sStroke.hasNext()) {
			GestureStroke a = inStroke.next();
			GestureStroke b = sStroke.next();
			
			if(!checkEndpoints(a,b)){
				if(DEBUG) Toast.makeText(this, "Endpoint mismatch", 
							0).show();
				return false;
			}
			
			int j = 0;
			int i = 0;
			
			// for each point in stroke a (of the input gesture)
			for(; i < a.points.length && j+1 < b.points.length; i+=2) {
				
				// check distance to points of stroke b in increasing order
				while(dist(a, b, i, j) > mSettings.getPathThresh()) {
					if(j+2 < b.points.length){
						// advance current point of b
						j += 2;
						
					} else {
						// ran out of points in the saved gesture's stroke
						// before verifying all points of the input gesture
						// were within the threshold -- invalid gesture
						if(DEBUG) Toast.makeText(this, "Path mismatch", 0).show();
						return false;
						
					}
				} 
			}
			
			// if for loop terminated before iterating over all points of a
			if(i != a.points.length){
				if(DEBUG) Toast.makeText(this, "Path mismatch", 0).show();
				return false;	// then stroke a != stroke b
			}
		}
		
		// neither gesture should have more strokes, else they're not equal
		boolean result = (!inStroke.hasNext() && !sStroke.hasNext());
		if(!result && DEBUG) Toast.makeText(this, "Path mismatch", 0).show();
		
		return result;
	}

	
	/*
	 * helper function for compareGestures(), checks that both end points
	 * of the given strokes are within END_THRESH
	 */
	private boolean checkEndpoints(GestureStroke a, GestureStroke b) {
		int a_end = a.points.length-2;
		int b_end = b.points.length-2;
		boolean result = true;
		
		// check starting points are near each other
		if(dist(a.points[0], a.points[1], 
				b.points[0], b.points[1]) > mSettings.getEndThresh())
			result = false;
		
		if( DEBUG ){
			float ax = a.points[0];
			float ay = a.points[1];
			float bx = b.points[0];
			float by = b.points[1];
			Log.d("points", "(a[0], a[1]) = (" + ax + ", " + ay + ")");
			Log.d("points", "(b[0], b[1]) = (" + bx + ", " + by + ")");
			Log.d("points", "dist = " + dist(ax,ay,bx,by));
		}
		
		// check ending points are near each other
		if( dist(a.points[a_end], a.points[a_end+1], 
				 b.points[b_end], b.points[b_end+1]) > mSettings.getEndThresh())
			result = false;
		
		return result;
	}
	
	
	/*
	 * helper function for checking distance between two points 
	 */
	private float dist(float ax, float ay, float bx, float by) {
		float dx = ax - bx;
		float dy = ay - by;
		return (float)Math.sqrt((double)(dx*dx + dy*dy));
	}

	
	/* 
	 * helper function for checking distance between the i-th point
	 * of gesture a and j-th point of gesture b
	 */
	private float dist(GestureStroke a, GestureStroke b, int i, int j) {
		float ax = a.points[i];
		float ay = a.points[i+1];
		float bx = b.points[j];
		float by = b.points[j+1];
		
		return dist(ax,ay,bx,by);
	}
	
	@Override
	public void onBackPressed() {
		// TODO save counts
		//saveMetrics();
		deleteFile("metrics");
		finish();
	}

	/*
	 * handle button behavior -- clear gesture or compare gesture
	 */
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.input_clear_button:
			gestureOverlay.setGesture(new Gesture());
			enterButton.setClickable(false);
			enterButton.setTextColor(0xff777777);
			break;
		case R.id.input_enter_button:
			Gesture gesture = gestureOverlay.getGesture();
			if(compareGestures(gesture)){
				SimpleDateFormat s = new SimpleDateFormat("ddMMyyhhmmss");
				Intent login = new Intent(this, LogInActivity.class);
				
				precisions.add(mSettings.precision);
				
				// put metrics
				float[] precs = new float[precisions.size()];
				for(int i = 0; i < precs.length; i++)
					precs[i] = precisions.get(i);

				login.putExtra(PRECISIONS, precs);
				login.putExtra(TIME_DATE, s.format(new Date()));
				login.putExtra(TRIES, tries+1);
				login.putExtra(GESTURES_COUNT, gestureCount);
				
				if(DEBUG) {
					String str = "" + (tries+1) + " " + precisions.size() + " " + gestureCount;
					Toast.makeText(this, str, 1).show();
				}
				
				// reset metrics
				gestureCount = 0;
				tries = 0;
				precisions.clear();
				
				if(mSettings.username!=null && mSettings.username.length()>0) {
					login.putExtra("username", mSettings.username);
				}
				
				if(mSettings.password!=null && mSettings.password.length()>0) {
					login.putExtra("password", mSettings.password);
				}
				
				
				startActivity(login);
			}
			else {
				tries++;
				AlertDialog failAlert = new AlertDialog.Builder(this).create();
				failAlert.setTitle("No Match");
				failAlert.setMessage("Gesture not recognized");
				failAlert.setButton("Dismiss", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {}
				});
				failAlert.show();
			}
			
			gestureOverlay.setGesture(new Gesture());
			enterButton.setClickable(false);
			enterButton.setTextColor(0xff777777);
			break;
		}
		
	}
	
	@Override  
	public boolean onCreateOptionsMenu(Menu menu) {  
		MenuInflater menuInflater = this.getMenuInflater();
		menuInflater.inflate(R.menu.input_options_menu, menu);
	    return super.onCreateOptionsMenu(menu);  
	} 
	
	/*
	 * Handles options menu behavior
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
		case R.id.input_options_configure:
			openConfiguration();
			break;
		case R.id.input_help:
			// TODO show pop up with some instructions
			AlertDialog helpAlert = new AlertDialog.Builder(this).create();
			helpAlert.setTitle("Help");
			helpAlert.setMessage("Configuration:\n" +
					"You must set your GatorLink Username and Password as well as " +
					"a gesture to use the application. You may also optionally " +
					"set an image as the background and set the difficulty." +
					"\n\nLogin:\n" +
					"To log-in to the ufw wireless network, the application " +
					"must be configured and your device must be connected to " +
					"ufw in the Wireless & network settings.\n" +
					"Next, draw your gesture and press the Enter button. The " +
					"app will open the login page and insert your username and " +
					"password. Then, finally, all you must do is press the Login button." +
					"\n\nTips:\n" +
					"-If concerned about privacy, you may change your GatorLink " +
					"email for the week while using the app." +
					"-Gestures may be multiple strokes.\n" +
					"-Strokes must start and end at the same points within some " +
					"threshold, based on the difficulty, to be recognized.\n");
			helpAlert.setButton("Dismiss", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {}
			});
			helpAlert.show();
			
			break;
		}
		
		return true;
	}
	
	/*
	 * helper function for opening ConfigurationActivity
	 */
	private void openConfiguration() {
		Intent config = new Intent(this, ConfigurationActivity.class);
		startActivity(config);
	}

	public void onGesture(GestureOverlayView overlay, MotionEvent event) {}


	public void onGestureCancelled(GestureOverlayView overlay, MotionEvent event) {}


	public void onGestureEnded(GestureOverlayView overlay, MotionEvent event) {}


	public void onGestureStarted(GestureOverlayView overlay, MotionEvent event) {
		if(mSettings.isValid()) {
			enterButton.setClickable(true);
			enterButton.setTextColor(0xff000000);
		}
	}

	private boolean fileExists(String filename) {
		String[] files = this.fileList();
		for(String f : files)
			if(f.compareTo(filename) == 0)
				return true;
		
		return false;
	}
}