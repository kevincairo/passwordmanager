/* Kevin Kyyro
 * HCI Project #3 - Gestural Password Manager Mock-up for Android
 * SetGestureActivity.java
 * 
 * Description: This is the activity for setting the gesture to
 * use for the configured password
 * --------------------------------------------------------------
 * TO-DO:
 * -add background image support
 * 
 * WISHLIST:
 * -practice mode?
 * 
 */

package kyyro.hci.password;

import android.app.Activity;
import android.content.Intent;
import android.gesture.Gesture;
import android.gesture.GestureOverlayView;
import android.gesture.GestureOverlayView.OnGestureListener;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class SetGestureActivity extends Activity 
	implements OnClickListener, OnGestureListener 
{
	private GestureOverlayView gestureOverlay;
	Button acceptButton;
	Button clearButton;
	private Intent result;
	private String bgPath;
	private boolean bg;
	private Bitmap bitmap;
	private BitmapDrawable bd;
	
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.set_gesture);
		
		bg = false;
		
		acceptButton = (Button)findViewById(R.id.set_accept_button);
		acceptButton.setOnClickListener(this);
		clearButton = (Button)findViewById(R.id.set_clear_button);
		clearButton.setOnClickListener(this);
		
		gestureOverlay = (GestureOverlayView)findViewById(R.id.set_gesture_overlay);
		gestureOverlay.addOnGestureListener(this);
		gestureOverlay.setFadeOffset(10000);
		gestureOverlay.setGestureStrokeAngleThreshold(0);
		gestureOverlay.setGestureStrokeLengthThreshold(0);
		gestureOverlay.setGestureStrokeSquarenessTreshold(0);
		gestureOverlay.setGestureStrokeType(1);
		gestureOverlay.setEventsInterceptionEnabled(false);
		
		Intent intent = getIntent();
		if(intent.hasExtra("path")) {
			bg = true;
			bgPath = intent.getStringExtra("path");
    		bitmap = BitmapFactory.decodeFile(bgPath);
    		bd = new BitmapDrawable(getResources(), bitmap);
    		gestureOverlay.setBackgroundDrawable(bd);
		}
		
		result = new Intent();
	}
	
	@Override
	public void finish() {
		/*Intent result = new Intent();
		result.putExtra(PasswordManagerActivity.GESTURE, 
			gestureOverlay.getGesture());*/
		setResult(0, result);
		super.finish();
	}
	
	 @Override
    protected void onDestroy() {
		super.onDestroy();
		if(bg) {
	    	super.onDestroy();
	    	bitmap.recycle();
	    	bd = null;
		}
		
    	
    }
	
	public void onClick(View v) {
		Gesture g = gestureOverlay.getGesture();
		if(g != null && g.getLength() > 0) {
			acceptButton.setClickable(true);
			acceptButton.setTextColor(0xff000000);
			clearButton.setClickable(true);
			clearButton.setTextColor(0xff000000);
		}
			
			
		switch(v.getId()) {
		case R.id.set_accept_button:	// only possible if gesture is non-null
			result.putExtra(PasswordManagerActivity.GESTURE, g);
			this.finish();
			break;
		case R.id.set_clear_button:
			gestureOverlay.setGesture(new Gesture());
			acceptButton.setClickable(false);
			acceptButton.setTextColor(0xff777777);
			clearButton.setClickable(false);
			clearButton.setTextColor(0xff777777);
			break;
		}

	}


	public void onGesture(GestureOverlayView overlay, MotionEvent event) {}


	public void onGestureCancelled(GestureOverlayView overlay, MotionEvent event) {
		/*acceptButton.setClickable(false);
		acceptButton.setTextColor(0xff777777);
		clearButton.setClickable(false);
		clearButton.setTextColor(0xff777777);*/
	}


	public void onGestureEnded(GestureOverlayView overlay, MotionEvent event){}


	public void onGestureStarted(GestureOverlayView overlay, MotionEvent event) {
		acceptButton.setClickable(true);
		acceptButton.setTextColor(0xff000000);
		clearButton.setClickable(true);
		clearButton.setTextColor(0xff000000);
		
	}

	/*public void onGesturePerformed(GestureOverlayView arg0, Gesture arg1) {
		//arg0.setGesture(arg1);
		acceptButton.setClickable(true);
		acceptButton.setTextColor(0xff000000);
		clearButton.setClickable(true);
		clearButton.setTextColor(0xff000000);
	}*/

}
