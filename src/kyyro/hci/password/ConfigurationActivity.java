/* Kevin Kyyro
 * HCI Project #3 - Gestural Password Manager Mock-up for Android
 * ConfigurationActivity.java
 * 
 * Description: This is the configuration activity. User must
 * configure the username, password, and gesture at the least.
 * --------------------------------------------------------------
 * TO-DO:
 * -complete onPause()
 * 		-store settings in persistent data
 * -complete onStart() method
 *	 	-load stored settings
 * -textual password confirmation for modifying gesture in separate
 *  configuration and for viewing gesture
 * -add check mark to completed fields
 * -rating option to rate each use
 * 
 * -report (2 pages, single spaced, 30 points):
 *  	*Task Analysis (5 points)
 * 		*User Level (Novice/Intermediate/Expert) Analysis (5 points)
 * 		*Applicability of 8 golden rules (15 points)
 * 		*Interface style choice (5 points)
 * 
 * WISHLIST:
 * -encryption
 * -slider for "tightness" of recognition instead of text input
 * -make prettier
 * 		-match aesthetic of phone settings
 * 		-number indicators on sliders
 * 		-background image preview
 * 		-zoom and pan background fit
 * -stroke color selector
 * 
 */

package kyyro.hci.password;

import java.io.IOException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.gesture.Gesture;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnKeyListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Toast;
import android.widget.SeekBar.OnSeekBarChangeListener;

public class ConfigurationActivity extends Activity 
	implements OnClickListener, OnSeekBarChangeListener, OnKeyListener,
		OnFocusChangeListener
{
	public static final String TAG = "ConfigurationActivity";
	
	// settings
	private MySettings mSettings;
	
	// ui elements
	private Button acceptButton;
	private EditText usernameBox;
	private EditText passwordBox;
	private Button clearUsernameButton;
	private Button clearPasswordButton;
	private Button setGestureButton;
	private Button resetAllButton;
	private SeekBar precisionSeekbar;
	private Button setBackgroundButton;
	private Button clearBackgroundButton;
	
	private boolean passwordIsOld;
	private String oldPassword;
	private boolean DEBUG = false;
	
	/*
	 * Responsible for populating menu with most current values
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setContentView(R.layout.configure);
        
        mSettings = new MySettings();
        mSettings.load(this);
        
        oldPassword = mSettings.password;
        initGUI();
        
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	}
	
	/*
	 * Get persistent data 
	 */
	@Override
    protected void onStart() {
        super.onStart();
        // The activity is about to become visible.
        
        // update UI to reflect settings
        updateGUI();
    }
    
	// INCOMPLETE -- all
    /*
     * Save persistent data
     * 
     */
    @Override
    protected void onPause() {
        super.onPause();
        // The activity is no longer visible (it is now "stopped")
        
        /* collect data from forms */
        // get username and password
     	mSettings.username = usernameBox.getText().toString();
     	mSettings.password = passwordBox.getText().toString();
        
        // update thresholds
     	float prog = (float)precisionSeekbar.getProgress();
     	mSettings.setPrecision(1.0f - prog/((float)precisionSeekbar.getMax()));
     	
     	// no longer saves on pause, saves on click accept
        //int result = mSettings.save(this);
        
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putString("username", usernameBox.getText().toString());
        savedInstanceState.putString("password", passwordBox.getText().toString());
        savedInstanceState.putInt("precision", precisionSeekbar.getProgress());
        
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        usernameBox.setText(savedInstanceState.getString("username"));
        passwordBox.setText(savedInstanceState.getString("password"));
        precisionSeekbar.setProgress(savedInstanceState.getInt("precision"));
    }
	
	/*
	 * find UI elements, initialize values, and set listeners
	 */
	private void initGUI() {
		acceptButton = (Button)findViewById(R.id.config_accept);
		acceptButton.setOnClickListener(this);
		
		usernameBox = (EditText)findViewById(R.id.username_box);
		usernameBox.setOnKeyListener(this);
		passwordBox = (EditText)findViewById(R.id.password_box);
		passwordBox.setOnKeyListener(this);
		
		passwordBox.setTypeface(Typeface.DEFAULT);
		passwordBox.setTransformationMethod(new PasswordTransformationMethod());
		
		clearUsernameButton = (Button)findViewById(R.id.clear_username_button);
		clearUsernameButton.setOnClickListener(this);
		clearPasswordButton = (Button)findViewById(R.id.clear_password_button);
		clearPasswordButton.setOnClickListener(this);
		
		setGestureButton = (Button)findViewById(R.id.set_gesture_button);
		setGestureButton.setOnClickListener(this);
		resetAllButton = (Button)findViewById(R.id.reset_all_button);
		resetAllButton.setOnClickListener(this);
		
		precisionSeekbar = (SeekBar)findViewById(R.id.precision_seekbar);
		precisionSeekbar.setOnSeekBarChangeListener(this);
		
		
		setBackgroundButton = (Button)findViewById(R.id.set_background_button);
		setBackgroundButton.setOnClickListener(this);
		clearBackgroundButton = (Button)findViewById(R.id.clear_background_button);
		clearBackgroundButton.setOnClickListener(this);
	}
	
	
	/*
	 * update UI elements to reflect values of mSettings
	 */
	private void updateGUI() {
        if(mSettings.username != null)
        	usernameBox.setText(mSettings.username);
        else
        	usernameBox.setText("");
        if(mSettings.password != null)
        	passwordBox.setText(mSettings.password);
        else
        	passwordBox.setText("");
        
     	precisionSeekbar.setProgress(
     			(int)((1.0f - mSettings.getPrecision())*precisionSeekbar.getMax()));
     	
     	// set clear background button's usability
     	if(mSettings.background){
     		clearBackgroundButton.setClickable(true);
     		clearBackgroundButton.setTextColor(0xff000000);
     	} else {
     		clearBackgroundButton.setClickable(false);
     		clearBackgroundButton.setTextColor(0xff777777);
     	}
	
        // set flag for whether password needs to be verified to
        // change gesture or view gesture
        if(mSettings.password == oldPassword)
        	passwordIsOld = true;
        else 
        	passwordIsOld = false;
	}
	
	// INCOMPLETE -- password confirmation & view gesture
	/* 
	 * Responsible for opening gesture setting activity, gesture viewing
	 * activity, image picker activity, reseting fields, etc.
	 * 
	 */
	public void onClick(View v) {
		switch(v.getId()) {
		case R.id.config_accept:
			// save settings and finish
			mSettings.username = usernameBox.getText().toString();
			mSettings.password = passwordBox.getText().toString();
			int result = mSettings.save(this);
			
			if(DEBUG)
	        	if(result < 0)
	        		Toast.makeText(this, "Save returned " + result, 0).show();
	        	else
	        		Toast.makeText(this, "Saved.", 0).show();
			
			finish();
			break;
		case R.id.clear_username_button:	// clear username
			mSettings.username = "";
			usernameBox.setText(mSettings.username);
			break;
		case R.id.clear_password_button:	// clear password
			mSettings.password = "";
			passwordBox.setText(mSettings.password);
			break;
			
		case R.id.set_gesture_button:		// create a gesture
			
			if(passwordIsOld) {
				// TODO if password is old, should verify password	
			}
			
			// then go to set gesture activity
			Intent setGesture = new Intent(this, SetGestureActivity.class);
			if(mSettings.background)
				setGesture.putExtra("path", mSettings.backgroundPath);
			
			startActivityForResult(setGesture, 
				PasswordManagerActivity.SET_GESTURE);
			
			break;
		case R.id.reset_all_button:			// reset all settings
			// add a confirmation
			AlertDialog resetAllAlert = new AlertDialog.Builder(this).create();
			resetAllAlert.setTitle("Reset All");
			resetAllAlert.setMessage("Clear all settings?");
			resetAllAlert.setButton("Okay", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int which) {
					/*reset all settings*/
					mSettings.reset();
					mSettings.setGesture(null);
					getApplicationContext().deleteFile("metricsl");
					updateGUI();
				}
			});
			
			resetAllAlert.setButton2("Cancel", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {}
			});
			
			resetAllAlert.show();
			
			try {
				this.getFileStreamPath("reset").createNewFile();
			} catch (IOException e) {
				Log.e(TAG, "reset: file not found", e);
			}
			break;
			
		case R.id.set_background_button:	// set background
			// launch a selector to choose an app to choose background with
			Intent intent = new Intent();
			intent.setType("image/*");
			intent.setAction(Intent.ACTION_GET_CONTENT);
			startActivityForResult(Intent.createChooser(intent, 
				"Complete action using:"), PasswordManagerActivity.SET_BACKGROUND);
			break;
			
		case R.id.clear_background_button:	// clear background
			AlertDialog clearBgAlert = new AlertDialog.Builder(this).create();
			clearBgAlert.setTitle("Clear Background");
			clearBgAlert.setMessage("Reset to default background?");
			clearBgAlert.setButton("Okay", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int which) {
					mSettings.background = false;
					mSettings.backgroundPath = null;
					updateGUI();
				}
			});
			
			clearBgAlert.setButton2("Cancel", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {}
			});
			
			clearBgAlert.show();
			break;
			
		}
		
	}

	/*
	 * if there is a numerical indicator, update it
	 */
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		//seekBar.getProgress();
		
	}
	
	/*
	 * not used
	 */
	public void onStartTrackingTouch(SeekBar seekBar) {}

	/*
	 * commit changes to mSettings
	 */
	public void onStopTrackingTouch(SeekBar seekBar) {
		// calculate threshold
		float t = 1.0f-((float)seekBar.getProgress()) / ((float)seekBar.getMax());
		//float thresh = (1.0f-t)*MySettings.MIN_THRESH + t*MySettings.MAX_THRESH;
		
		// update appropriate threshold
		/*if(seekBar.getId() == R.id.path_thresh_bar)
		switch(seekBar.getId()){
		case R.id.path_thresh_bar:
			mSettings.setPathThresh(thresh);
			break;
		case R.id.end_thresh_bar:
			mSettings.setEndThresh(thresh);
			break;
		case R.id.precision_seekbar:
			mSettings.precision = thresh;
			break;
		}*/
		
		if(seekBar.getId() == R.id.precision_seekbar)
			mSettings.setPrecision(t);
		
	}

	/*
	 * Records changes to username and password when user presses enter
	 */
	public boolean onKey(View v, int keyCode, KeyEvent event) {
		// if user presses enter
		if((event.getAction() == KeyEvent.ACTION_UP) &&
			(keyCode == KeyEvent.KEYCODE_ENTER) ) {
			
			// no longer do this because the text boxes have TextWatchers
			/*// set the changed field
			if(v.getId() == R.id.username_box)
				mSettings.username = usernameBox.getText().toString();		
			else
				mSettings.password = passwordBox.getText().toString();*/
			
			// hide keyboard
			InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			mgr.hideSoftInputFromWindow(v.getWindowToken(), 0);
			
			return true;
		}
		
		return false;
	}
	
	@Override
	public void onBackPressed() {
		AlertDialog backAlert = new AlertDialog.Builder(this).create();
		backAlert.setTitle("Returning");
		backAlert.setMessage("Would you like to save your changes?");
		backAlert.setButton("Yes", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				mSettings.username = usernameBox.getText().toString();
				mSettings.password = passwordBox.getText().toString();
				mSettings.save(getApplicationContext());
				finish();
			}
		});
		
		backAlert.setButton2("No thanks", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {finish();}
		});
		
		backAlert.show();
	}
	
	/*
	 * Records changes to username or password on focus change
	 */
	public void onFocusChange(View v, boolean hasFocus) {
		// only interested when focus lost
		if(v.getId() != R.id.password_box && v.getId() != R.id.username_box){
			InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(usernameBox.getWindowToken(), 0);
		}
	}
	
	
	// INCOMPLETE -- set background results
	protected void onActivityResult(int requestCode, int resultCode, Intent i) {
		Log.d(TAG, "result = " + resultCode);
		
		// set gesture results
		if(requestCode == PasswordManagerActivity.SET_GESTURE) {
			if(i.hasExtra(PasswordManagerActivity.GESTURE)) {
				Gesture resultGesture = 
					(Gesture)i.getExtras().get(PasswordManagerActivity.GESTURE);
				
				// only want to replace with non-null gestures
				if(resultGesture.getLength() > 0) {
					mSettings.setGesture(resultGesture);
					
					if(DEBUG) Toast.makeText(this, "Gesture non zero", 0).show();
				} else {
					if(DEBUG) Toast.makeText(this, "Gesture is zero.", 0).show();
				}
			}
			 
		/* INCOMPLETE */
		// set background results
		} else if(requestCode == PasswordManagerActivity.SET_BACKGROUND &&
				resultCode == Activity.RESULT_OK) {
			Uri mImage = i.getData();
			String[] proj = {MediaStore.Images.Media.DATA};
			Cursor cursor = managedQuery( mImage, proj, null, null, null );
			
			if(cursor == null){	// from File Manager
				mSettings.backgroundPath = mImage.getPath();
			} else {			// from Gallery
				int ind = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
				cursor.moveToFirst();
				mSettings.backgroundPath = cursor.getString(ind);
				
			}
			
			if(mSettings.backgroundPath != null) {
				mSettings.background = true;
				if(DEBUG) Toast.makeText(this, mSettings.backgroundPath, 0).show();
				
			} else if(DEBUG){
				Toast.makeText(this, "no background path", 0).show();
			}
		} else if(DEBUG)
			Toast.makeText(this, "Result unrecognized: " + requestCode, 0).show();
	}

}
