package kyyro.hci.password;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class LogInActivity extends Activity
	implements DialogInterface.OnDismissListener {
	public static final String TAG = "LogInActivity";
	
	private WebView mWebView;
	private String username;
	private String password;
	private final String loginUrl = "ns.ufl.edu/auth/perfigo_weblogin.jsp";
	private final String successUrl = "ns.ufl.edu/auth/perfigo_cm_validate.jsp";
	private final String[] email = {"kmkyyro@ufl.edu"};
	
	private boolean DEBUG = false;
	private Handler handler;
	
	AlertDialog alertDialog;
	
	
	public void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.log_in_screen);
		
		handler = new Handler();
		
		// prepare webView
		mWebView = (WebView)findViewById(R.id.login_web_view);
		mWebView.getSettings().setJavaScriptEnabled(true);
		LogInWebViewClient client = new LogInWebViewClient();
		mWebView.setWebViewClient(client);
	}

	protected void onStart() {
		super.onStart();
		alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setOnDismissListener(this);
		
		// get username and password if they're configured
		Intent intent = this.getIntent();
		
		if(intent.hasExtra("username") && intent.hasExtra("password")) {
			username = intent.getStringExtra("username");
			password = intent.getStringExtra("password");
		} else {
			alertDialog.setTitle("No Username/Password");
			alertDialog.setMessage("Please configure a username and password.");
			alertDialog.setButton("Dismiss", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					finish();
				}
			});
			alertDialog.show();
			return;
		}
		
		
		// load login page
		mWebView.loadUrl("http://www.ufl.edu");
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if ((keyCode == KeyEvent.KEYCODE_BACK) && mWebView.canGoBack()) {
	        //mWebView.goBack();
	    	if(DEBUG) {
				mWebView.loadUrl("javascript: {" +
					"document.getElementById('passwordCell').innerHTML='poop';" +
					"};");
			}
	        return true;
	    }
	    return super.onKeyDown(keyCode, event);
	}
	
	private void emailDialog() {
		alertDialog.setTitle("Send Log-In Confirmation");
		alertDialog.setMessage("An e-mail will be created with some " +
				"statistics about your usage. You will be cc'd at your " +
				"ufl email address so you know what kind of data is being " +
				"collected. Feel free to add comments on your experience " +
				"to the end of the email." +
				"\n\nNOTE: you must send the email to verify you " +
				"are using the application to be elligible for the " +
				"gift card drawing. Thanks!");
		alertDialog.setButton("Okay", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				sendEmail();
			}
		});
		alertDialog.setButton2("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				alertDialog.dismiss();
			}
		});
		alertDialog.show();
	}
	
	/*
	 * Send confirmation email
	 */
	private void sendEmail() {
		Intent caller = getIntent();
		String timeDate = caller.getStringExtra(PasswordManagerActivity.TIME_DATE);
		int tries = caller.getIntExtra(PasswordManagerActivity.TRIES, -1);
		int gCount = caller.getIntExtra(PasswordManagerActivity.GESTURES_COUNT, -1);
		float[] precisions = 
			caller.getFloatArrayExtra(PasswordManagerActivity.PRECISIONS);
		
		String precStr = "";
		if(precisions != null) {
			for(Float f : precisions)
				precStr += " " + f;
		} else {
			precStr = " null";
		}
		
		String body = "Date: " + timeDate + "\n" +
				"Tries: " + tries + "\n" +
				"Number of gesture changes: " + gCount + "\n" +
				"Precisions:" + precStr;
		int hash = hash(body);
		String subject = "User Study -- #" + hash;
		Intent emailIntent = new Intent(Intent.ACTION_SEND);
		emailIntent.putExtra(Intent.EXTRA_EMAIL, email);
		emailIntent.putExtra(Intent.EXTRA_CC, new String[]{username+"@ufl.edu"});
		emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
		emailIntent.putExtra(Intent.EXTRA_TEXT, body + 
				"\n\n***Feel free to add any comments below***\n");
		emailIntent.setType("text/plain");
		startActivity(Intent.createChooser(emailIntent, "Sending E-mail"));
	}
	
	/*
	 * Simple hash function to make sure user isn't fabricating log-in email
	 */
	private int hash(String s) {
		int result = 0;
		byte[] bytes = s.getBytes();
		for(byte b : bytes)
			result += b*b;
		
		return result;
	}
	
	private void alreadyLoggedIn() {
		Toast.makeText(this, "Already logged into ufw.", 0).show();
	}
	
	private class LogInWebViewClient extends WebViewClient {
		/*
		 * Called when a link is clicked, want to limit behavior to log-in 
		 */
		@Override
	    public boolean shouldOverrideUrlLoading(WebView view, String url) {
	    	Log.d(TAG, "Loading: " + url);
			view.loadUrl(url);
	        return true;
	    }
		
		@Override
		public void onPageFinished(WebView view, String url) {
			// if logging in, fill form
			if(url.contains(loginUrl)) {
				view.loadUrl("javascript: {" +
					"top.left.document.getElementsByName('username')[0].value='" + username + "';" +
					"top.left.document.getElementsByName('password')[0].value='" + password + "';" +
					"};");
				
			// else if success
			} else if (url.contains(successUrl)) {
				if(DEBUG)
					Toast.makeText(getApplicationContext(), "Success!!!", 0).show();
				
				// send confirmation email from UI thread
				handler.post(new Runnable() {
					public void run() {
						emailDialog();
					}
				});
				
			} else if (url.compareTo("http://m.ufl.edu/") == 0) {
				handler.post(new Runnable(){public void run(){alreadyLoggedIn();}});
			}
			Log.d(TAG, "Loaded: " + url);
		}
		
		@Override
		public void onReceivedError(WebView view, int errorCode, 
				String description, String failingUrl) {
			
			Log.d(TAG, "LoginError: " + errorCode + ", " + description +
					", @" + failingUrl);
			if(failingUrl.compareTo(loginUrl) == 0 && 
					errorCode == WebViewClient.ERROR_CONNECT) {
				// display failure message
				//handler.post(new Runnable(){public void run(){loginFailed();}});
				Toast.makeText(getApplicationContext(), "Did you connect " +
						"to ufw in your Network & wireless settings?", 0).show();
			}
				
		}
		
	}

	public void onDismiss(DialogInterface dialog) { 
		finish();
	}
}
