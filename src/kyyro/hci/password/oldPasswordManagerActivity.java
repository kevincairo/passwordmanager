/* Kevin Kyyro
 * HCI Project #3 - Gestural Password Manager Mock-up for Android
 * Description: allows user to associate a gesture or "drawing"
 * with a username and password so that password input can be
 * executed quickly and with immunity to shoulder surfing.
 * --------------------------------------------------------------
 * -parameters: thresh 35, end thresh 50
 * 		-note: path should be tighter, end points can be a looser
 * 
 * TO-DO:
 * -add parameters to allow a number of deviations from the path for
 *  a maximum number of points
 * -add password storage
 * -allow user to add a picture for the background
 * 
 * -report (2 pages, single spaced, 30 points):
 *  	*Task Analysis (5 points)
 * 		*User Level (Novice/Intermediate/Expert) Analysis (5 points)
 * 		*Applicability of 8 golden rules (15 points)
 * 		*Interface style choice (5 points)
 * 
 * WISHLIST:
 * -add in app browser so that users can actually use gestures to
 *  sign into UF wifi
 * -requires adding some reasonable amount of encryption to the 
 *  stored passwords...
 * 
 */

package kyyro.hci.password;

import java.util.Iterator;
import java.lang.Math;

import android.app.Activity;
import android.gesture.Gesture;
import android.gesture.GestureOverlayView;
import android.gesture.GestureOverlayView.OnGesturePerformedListener;
import android.gesture.GestureStroke;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class oldPasswordManagerActivity extends Activity 
	implements OnGesturePerformedListener, OnClickListener {
    
	private float THRESH = 10.0f;
	private float END_THRESH = 1.0f;
	private boolean DEBUG = true;
	
	//private GestureLibrary mLibrary;
	private Gesture lastGesture;
	private Gesture savedGesture;
	
	GestureOverlayView gestureOverlay;
	private Button resetButton;
	private Button configButton;
	private EditText threshBox;
	private EditText endThreshBox;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.input);
        
        //mLibrary = null;
        
        gestureOverlay = 
        	(GestureOverlayView)findViewById(R.id.gestures);
        gestureOverlay.addOnGesturePerformedListener(this);
        gestureOverlay.setFadeOffset(1000);
        gestureOverlay.setGestureStrokeLengthThreshold(0);
        gestureOverlay.setGestureStrokeAngleThreshold(0);
        gestureOverlay.setGestureStrokeSquarenessTreshold(0);
        
        resetButton = (Button) findViewById(R.id.reset_button);
        resetButton.setOnClickListener(this);
        configButton = (Button) findViewById(R.id.config_button);
        configButton.setOnClickListener(this);
        threshBox = (EditText) findViewById(R.id.threshold_box);
        threshBox.setText(Float.toString(THRESH));
        endThreshBox = (EditText) findViewById(R.id.end_threshold_box);
        endThreshBox.setText(Float.toString(END_THRESH));
    }

	public void onGesturePerformed(GestureOverlayView overlay, Gesture gesture) {
		lastGesture = gesture;
		
		if(compareGestures(gesture))
			Toast.makeText(this, "Match! :)", Toast.LENGTH_SHORT).show();
		//else if(savedGesture != null)
		//	Toast.makeText(this, "No match. :(", Toast.LENGTH_SHORT).show();
	}
	
	// check if input gesture matches saved gesture 
	boolean compareGestures(Gesture gesture) {
		if(savedGesture == null || gesture == null)
			return false;
		
		updateThresh();	// parse threshold from text box
		
		Iterator<GestureStroke> inStroke = gesture.getStrokes().iterator();
		Iterator<GestureStroke> sStroke = savedGesture.getStrokes().iterator();
		
		// for each stroke of both gestures
		while(inStroke.hasNext() && sStroke.hasNext()) {
			GestureStroke a = inStroke.next();
			GestureStroke b = sStroke.next();
			
			if(!checkEndpoints(a,b)){
				Toast.makeText(this, "Endpoint mismatch", 
					Toast.LENGTH_SHORT).show();
				return false;
			}
			
			int j = 0;
			int i = 0;
			
			// for each point in stroke a (of the input gesture)
			for(; i < a.points.length && j+1 < b.points.length; i+=2) {
				float ax = a.points[i];
				float ay = a.points[i+1];
				float bx = b.points[j];
				float by = b.points[j+1];
				
				// check distance to points of stroke b in increasing order
				while(dist(ax,ay,bx,by) > THRESH) {
					if(j+2 < b.points.length){
						// get next point of b
						j += 2;
						bx = b.points[j];
						by = b.points[j+1];
						
					} else {
						// ran out of points in the saved gesture's stroke
						// before verifying all points of the input gesture
						// were within the threshold -- invalid gesture
						Toast.makeText(this, "Path mismatch", 
							Toast.LENGTH_SHORT).show();
						return false;
						
					}
				} 
			}
			
			// if for loop terminated before iterating over all points of a
			if(i != a.points.length){
				Toast.makeText(this, "Path mismatch", Toast.LENGTH_SHORT).show();
				return false;	// then stroke a != stroke b
			}
		}
		
		// neither gesture should have more strokes, else they're not equal
		boolean result = (!inStroke.hasNext() && !sStroke.hasNext());
		if( !result )
			Toast.makeText(this, "Path mismatch", Toast.LENGTH_SHORT).show();
		
		return result;
	}

	private boolean checkEndpoints(GestureStroke a, GestureStroke b) {
		int a_end = a.points.length-2;
		int b_end = b.points.length-2;
		boolean result = true;
		
		// check starting points are near each other
		if(dist(a.points[0], a.points[1], 
				b.points[0], b.points[1]) > END_THRESH)
			result = false;
		
		if( DEBUG ){
			float ax = a.points[0];
			float ay = a.points[1];
			float bx = b.points[0];
			float by = b.points[1];
			Log.d("points", "(a[0], a[1]) = (" + ax + ", " + ay + ")");
			Log.d("points", "(b[0], b[1]) = (" + bx + ", " + by + ")");
			Log.d("points", "dist = " + dist(ax,ay,bx,by));
		}
		
		// check ending points are near each other
		if( dist(a.points[a_end], a.points[a_end+1], 
				 b.points[b_end], b.points[b_end+1]) > END_THRESH)
			result = false;
		
		return result;
	}
	
	private float dist(float ax, float ay, float bx, float by) {
		float dx = ax - bx;
		float dy = ay - by;
		return (float)Math.sqrt((double)(dx*dx + dy*dy));
	}
	
	private void updateThresh() {
		// update threshold
		float temp = THRESH;
		try{
			temp = Float.parseFloat(threshBox.getText().toString());
		} catch(NumberFormatException nfex) {
			temp = THRESH;
		}
		
		THRESH = temp;
		
		// update endpoint threshold
		temp = END_THRESH;
		try{
			temp = Float.parseFloat(endThreshBox.getText().toString());
		} catch(NumberFormatException nfex) {
			temp = END_THRESH;
		}
		
		END_THRESH = temp;
	}

	public void onClick(View v) {
		switch(v.getId()){
		case R.id.reset_button:
			gestureOverlay.setGesture(null);
			break;
		case R.id.config_button:
			savedGesture = lastGesture;
			
			if(savedGesture != null)
				Toast.makeText(this, "Saved.", Toast.LENGTH_SHORT).show();
			break;
		}
		
	}
}