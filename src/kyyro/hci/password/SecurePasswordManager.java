package kyyro.hci.password;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import android.util.Log;

/*
 * Helper class for file encryption and decryption
 */
public class SecurePasswordManager {
	public static final String TAG = "SecurePasswordManager";
	private String seed;
	
	SecurePasswordManager(String aSeed) {
		seed = toHex(aSeed);
	}
	
	public String encrypt(String data){
		//return crypt(data.getBytes(), Cipher.ENCRYPT_MODE);
		return data;
	}
	
	public String decrypt(String data){		
		//return crypt(toByte(data), Cipher.DECRYPT_MODE);
		return data;
	}
	
	private String crypt(byte[] data, int mode) {
		try {
			KeyGenerator keygen = KeyGenerator.getInstance("AES");
			SecureRandom rand = SecureRandom.getInstance("SHA1PRNG");
			rand.setSeed(seed.getBytes());
			keygen.init(128, rand);
			
			SecretKeySpec keyspec = 
				new SecretKeySpec(keygen.generateKey().getEncoded(), "AES");
			Cipher cipher = Cipher.getInstance("AES");
			cipher.init(mode, keyspec);
			
			if(mode == Cipher.ENCRYPT_MODE)
				return toHex(cipher.doFinal(data));
			else
				return new String(cipher.doFinal(data));
			
		} catch (NoSuchAlgorithmException nsaex) {
			Log.e(TAG, "No algorithm for key generation", nsaex);
		} catch (NoSuchPaddingException nspex) {
			Log.e(TAG, "No such padding for cipher", nspex);
		} catch (InvalidKeyException ikex) {
			Log.e(TAG, "Invalid key for cipher", ikex);
		} catch (IllegalBlockSizeException e) {
			Log.e(TAG, "Illegal block size for cipher.doFinal()", e);
		} catch (BadPaddingException e) {
			Log.e(TAG, "Bad padding for cipher.doFinal()", e);
		}
		
		return "%Error%";
	}
	
	public static String toHex(String txt) {
		return toHex(txt.getBytes());
	}
	public static String fromHex(String hex) {
		return new String(toByte(hex));
	}
	
	public static byte[] toByte(String hexString) {
		int len = hexString.length()/2;
		byte[] result = new byte[len];
		for (int i = 0; i < len; i++)
			result[i] = Integer.valueOf(hexString.substring(2*i, 2*i+2), 16).byteValue();
		return result;
	}

	public static String toHex(byte[] buf) {
		if (buf == null)
			return "";
		StringBuffer result = new StringBuffer(2*buf.length);
		for (int i = 0; i < buf.length; i++) {
			appendHex(result, buf[i]);
		}
		return result.toString();
	}
	private final static String HEX = "0123456789ABCDEF";
	private static void appendHex(StringBuffer sb, byte b) {
		sb.append(HEX.charAt((b>>4)&0x0f)).append(HEX.charAt(b&0x0f));
	}
}
